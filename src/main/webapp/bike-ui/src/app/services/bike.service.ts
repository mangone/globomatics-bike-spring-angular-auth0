import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
};

@Injectable()
export class BikeService {

	constructor(private http:HttpClient) { }

	findAll(){
		let token = localStorage.getItem('access_token');
		return this.http.get('/server/bikes',
			{headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)}
		);
	}

	create(bike){
		let body = JSON.stringify(bike);
		return this.http.post('/server/bikes', body, httpOptions );
	}

	get(id: number){
		let token = localStorage.getItem('access_token');
		return this.http.get('/server/bikes/'+id,
			{headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)}
		);
	}
}
