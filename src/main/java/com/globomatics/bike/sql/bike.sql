CREATE TABLE bike(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	contact BOOLEAN NOT NULL,
	email VARCHAR,
	model VARCHAR,
	name VARCHAR,
	phone VARCHAR,
	purchase_date DATETIME,
	purchase_price NUMERIC,
	serial_number VARCHAR
	);

INSERT INTO bike (contact, email, model, name, phone, purchase_price) VALUES
(1, 'jeff@bikes.com', 'Globo MTB 29 Full Suspension', 'Jeff Miller', '328-443-5555', '1100'),
(0, 'samantha@bikes.com', 'Globo Carbon Fiber Race Series', 'Samantha Davis', '448-397-5555', '1999'),
(1, 'dave@bikes.com', 'Globo Time Trial Blade', 'Dave Warren', '563-891-5555', '2100');